# GelirGider

## Genel pencere görünümü
Proje sonunda aşağıdaki resimde gördüğünüz gibi basit bir program yazmış olacağız.


## PyGtk Kurulumu

Büyük olasılıkla yüklediğiniz dağıtımda PyGtk kurulu olacaktır ancak kurmak isterseniz. Aşağıdaki komutları çalıştırabilirsiniz.

### Debian Tabanlı Dağıtımlar (Ubuntu, Linux Mint)
```console
sudo apt install python3 python3-gi python3-gi-cairo gir1.2-gtk-3.0
```

### Fedora

```console
sudo dnf install python3 python3-gi python3-gi-cairo gir1.2-gtk-3.0
```

### Arch Linux

```console
sudo pacman -S python3 python3-gi python3-gi-cairo gir1.2-gtk-3.0
```

## main.py başlangıç

```python
#!/usr/bin/python3
# Dosyanın çalıştırılacağı program yolu

#temel gtk kütüphanesini import ediyoruz.
#argümanları almak için sys kütüphanesini import ediyoruz
import gi, sys
#sistemde farklı gtk versiyonları olması durumunda hangi versiyonu beklediğimizi belirtiyoruz.
gi.require_version("Gtk","3.0")

#Eğer uygun versiyon sistemde varsa Gtk'yı import ediyoruz
from gi.repository import Gtk
```

```python
class Application(Gtk.Application):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, application_id="gitlab.com.sonakinci41.gelirgider", flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE, **kwargs)
		GLib.set_application_name("FTerm")
		GLib.set_prgname('fterm')
		self.settings = GLib.KeyFile()

		self.win = None
		self.add_main_option("fullscrenn", ord("f"), GLib.OptionFlags.NONE, GLib.OptionArg.STRING, "FullScreen", None)
		#self.add_main_option("new", ord("n"), GLib.OptionFlags.NONE, GLib.OptionArg.STRING, "NewWindow", None)

	def do_start_up(self):
		# Fonksiyon ilk çalıştırmada çalışır bunun için süreçleri başlatalım.
		Gtk.Application.do_startup(self)

	def do_activate(self):
		pass

	def do_command_line(self, command_line):
		pass
```

```python
# Diğer dillerden kalma alışkanlıklar.
if __name__ == '__main__':
	# Uygulama sınıfını oluşturuyoruz
	app = Application()
	#sys ile gelen argümanları uygulamaya gönderiyoruz
	app.run(sys.argv)
```