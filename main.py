#!/usr/bin/python3

import gi, sys
gi.require_version("Gtk","3.0")


from gi.repository import Gtk, Gio, GLib, GdkPixbuf


class MainWindow(Gtk.ApplicationWindow):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

		# PENCERE SİMGESİ
		try:
			icon = GdkPixbuf.Pixbuf.new_from_file("/usr/share/icons/hicolor/scalable/apps/gg.svg")
		except:
			icon = GdkPixbuf.Pixbuf.new_from_file("./icons/gg.svg")
		self.set_icon(icon)

		# HEADER BAR VE TİTLE
		header_bar = Gtk.HeaderBar()
		self.set_titlebar(header_bar)
		header_bar.set_show_close_button(True)
		header_bar.set_title("GelirGider")


		ge_box = Gtk.VBox()
		gi_box = Gtk.VBox()
		i_box = Gtk.VBox()


		# MAİN PANNED
		self.main_panned = Gtk.VPaned()
		self.add(self.main_panned)

		# MAİN PANNED
		self.v_panned = Gtk.HPaned()
		self.main_panned.pack1(self.v_panned,False,False)
		self.main_panned.pack2(i_box,False,False)

		self.v_panned.pack1(ge_box,False,False)
		self.v_panned.pack2(gi_box,False,False)

		# GELİR TREE VİEW
		self.ge_store = Gtk.ListStore(GdkPixbuf.Pixbuf(),str)
		self.ge_tree = Gtk.TreeView(model=self.ge_store)
		#self.ge_tree.connect("button-release-event",self.right_click_menu)

		ge_icon = Gtk.CellRendererPixbuf()
		ge_icon.set_fixed_size(32,32)
		icon = Gtk.TreeViewColumn("Icon",ge_icon,gicon = 0)
		self.ge_tree.append_column(icon)

		ge_text = Gtk.CellRendererText()
		text_2 = Gtk.TreeViewColumn("Text",ge_text, text = 1)
		self.ge_tree.append_column(text_2)

		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.ge_tree)
		ge_box.pack_start(scroll,True, True, 5)


		# GİDER TREE VİEW
		self.gi_store = Gtk.ListStore(GdkPixbuf.Pixbuf(),str)
		self.gi_tree = Gtk.TreeView(model=self.gi_store)
		#self.gi_tree.connect("button-release-event",self.right_click_menu)

		gi_icon = Gtk.CellRendererPixbuf()
		gi_icon.set_fixed_size(32,32)
		icon = Gtk.TreeViewColumn("Icon",gi_icon,gicon = 0)
		self.gi_tree.append_column(icon)

		gi_text = Gtk.CellRendererText()
		text_2 = Gtk.TreeViewColumn("Text",gi_text, text = 1)
		self.gi_tree.append_column(text_2)

		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.gi_tree)
		gi_box.pack_start(scroll,True, True, 5)





class Application(Gtk.Application):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, application_id="gitlab.com.sonakinci41.gelirgider", flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE, **kwargs)
		GLib.set_application_name("GelirGider")
		GLib.set_prgname('gg')
		self.settings = GLib.KeyFile()

		self.win = None
		self.add_main_option("fullscrenn", ord("f"), GLib.OptionFlags.NONE, GLib.OptionArg.STRING, "FullScreen", None)
		self.add_main_option("username", ord("u"), GLib.OptionFlags.NONE, GLib.OptionArg.STRING, "UserName", None)


	def do_start_up(self):
		Gtk.Application.do_startup(self)

	def do_activate(self):
		if not self.win:
			self.win = MainWindow(application=self)
			self.win.connect("delete-event", self.write_gg_settings)
			self.win.connect("destroy", Gtk.main_quit)
			self.read_gg_settings()
			self.win.show_all()

	def do_command_line(self, command_line):
		options = command_line.get_options_dict()
		options = options.end().unpack()
		for key in options.keys():
			if key == "username":
				print("Programı bu kullanıcı ile çalıştır {}".format(options[key]))
			elif key == "fullscrenn":
				print("Tam ekran başlat {}".format(options[key]))
		if self.win == None:
			self.do_activate()

	def write_gg_settings(self, win, x):
		pass

	def read_gg_settings(self):
		pass



if __name__ == '__main__':
	app = Application()
	app.run(sys.argv)